

// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/25 ,Program Name: midterm test


package exams.first;

public class Pet {
	private String name;
	private int age;
	
	public Pet() {
		name = "Baba";
		age = 1;
	}
	
	public Pet(String name, int age) {
		this.name = name;
		age = 1;
		setAge(age);
	}
	
	public void setName(String name) {
		this.name = name ;
	}
	
	public void setAge(int age) {
		if(this.age > 0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	
	
	public boolean equals(Object obj) {
		
		if(obj == null ) {
			return false;
		}
		
		if (!(obj instanceof Pet)) {
			return false;
		}
		
		Pet a = (Pet)obj;
		return this.getName().equals(a.getName()) && this.getAge() == a.getAge();
	}
		
		
	public String toString() {
	   return "The Pet name is " + name + "  and age is  " + age;
	}
	
	
}
