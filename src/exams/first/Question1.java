
// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/25 ,Program Name: midterm test


package exams.first;

import java.util.Scanner;

public class Question1 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		try {
			System.out.println("Please Enter an Integer : ");
			int i = Integer.parseInt(input.nextLine());
			int x = i + 65;
			char j = (char)x; // typecast 
			System.out.println(j);	
		}catch(NumberFormatException e) {
			System.out.println("Your input is not an integer!!!");
		} 
		
			input.close();	
				

	}

}
