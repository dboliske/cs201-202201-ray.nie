

// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/25 ,Program Name: midterm test

package exams.first;

import java.util.Scanner;

public class Question3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		try {
			System.out.println("Input an integer to make Triangle： ");
			
			int z = Integer.parseInt(input.nextLine());
			
			for(int i = 0 ; i <z ;i++){
				for (int j = 0; j < z; j++) {
					if (j >= i) {
						System.out.print("*");
					}else {
						System.out.print(" ");
					}
				}
				System.out.println();
			}
				
		}catch(NumberFormatException e) {
			System.out.println("Your input is not an integer!!!");
		}
	
		input.close();

	}

}
