package exams.first;
import java.util.Scanner;

// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/25 ,Program Name: midterm test



public class Question4 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        WordCount[] line = new WordCount[5];

        int a = 0;
        for (int i = 0; i < 5; i++) {

            System.out.println("Input 5 words and now it is  " + (i + 1) + " Word : ");
            String word = input.nextLine();

            Boolean newAdd = true;
            for (int z = 0; z < 5; z++) {
                if (line[z] != null && line[z].getWord().equals(word)) {
                    line[z].setNum(line[z].getNum() + 1);
                    newAdd = false;
                    break;
                }
            }

            if (newAdd) {
                line[a] = new WordCount(word, 1);
                a++;
            }
        }

        for (int z = 0; z < line.length; z++) {
            if (line[z] != null && line[z].getNum() > 1) {
                System.out.println("The word that appears more than once is " + line[z].getWord());
             
            }
        }

        input.close();

    }

}


class WordCount {

    private String word;
    private int num;

    public WordCount(String word, int num) {
        this.word = word;
        this.num = num;
    }


    public void setWord(String word) {
        this.word = word;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getWord() {
        return this.word;
    }

    public int getNum() {
        return this.num;
    }


}