

//name: RUICHAO NIE  ;	course: CS201;	sec: 03
//date: 04/30/2022; 	name of project: exams.second

package exams.second;

import java.awt.Polygon;

public class Circle extends Polygon {

	private double radius;

	public Circle() {
		super();
		radius = 1;
	}

	/**
	 * @return the radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(double radius) {
		if (radius > 0) {
			this.radius = radius;
		}

	}

	@Override
	public String toString() {
		return super.toString() + "Circle [radius=" + radius + "]";
	}

	public double area() {
		return (Math.PI * radius * radius);
	}

	public double perimeter() {
		return (2.0 * Math.PI * radius);
	}

}
