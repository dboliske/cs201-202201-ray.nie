//name: RUICHAO NIE  ;	course: CS201;	sec: 03
//date: 04/30/2022; 	name of project: exams.second

package exams.second;

public class ComputerLab extends Classroom {
	private boolean computers;
	
	public ComputerLab() {
		super();
	}

	public boolean hasComputers() {
		return computers;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}

	@Override
	public String toString() {
		return "ComputerLab [computers=" + computers +super.toString() +"]";
	}
	
	

	
}
