# Final Exam

## Total

93/100

## Break Down

1. Inheritance/Polymorphism:    20/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  5/5
2. Abstract Classes:            15/20
    - Superclass:               5/5
    - Subclasses:               2/5
    - Variables:                5/5
    - Methods:                  3/5
3. ArrayLists:                  19/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        19/20
    - Compiles:                 5/5
    - Jump Search:              10/10
    - Results:                  4/5

## Comments

1. ok
2. subclass import wrong Ploygon superclass, -3. 
   No override identifier on area() and permiter on subclass, -2
3. Error when the first input is 'done', -1
4. ok
5. didn't print -1 when the value didn't present. -1