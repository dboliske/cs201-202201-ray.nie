
//name: RUICHAO NIE  ;	course: CS201;	sec: 03
//date: 04/30/2022; 	name of project: exams.second


package exams.second;

public abstract class Ploygon {
	protected String name;

	public Ploygon() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public abstract String toString();

	public abstract double area();

	public abstract double perimeter();

}
