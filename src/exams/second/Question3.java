//name: RUICHAO NIE  ;	course: CS201;	sec: 03
//date: 04/30/2022; 	name of project: exams.second

package exams.second;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Question3 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		List<Double> array = new ArrayList<>();
		String str = null;

		boolean flag = false;

		do {
			System.out.println("Please enter a number and Exit by Done：");
			str = input.nextLine();
			
			if (str.equalsIgnoreCase("done")) {
				flag = true;
			} else {
				try {
					double newnums = Double.parseDouble(str);
					array.add(newnums);
				} catch (Exception e) {
					System.out.println("I dont know what you enter !");
				}
			}

		} while (!flag);

		double maxNumber = Collections.max(array);
		double minNumber = Collections.min(array);

		System.out.println("The max is " + maxNumber + " . " + " The min is " + minNumber);

		input.close();
	}

}
