//name: RUICHAO NIE  ;	course: CS201;	sec: 03
//date: 04/30/2022; 	name of project: exams.second

package exams.second;

public class Question4 {

	public static String[] selectSort(String[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			int min = i;

			for (int j = i + 1; j < array.length; j++) {
				if (array[j].compareTo(array[min]) < 0) {
					min = j;
				}
			}

			if (min != i) {
				String temp = array[i];

				array[i] = array[min];

				array[min] = temp;
			}

		}

		return array;
	}

	public static void main(String[] args) {
		String[] lang = { "speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage",
				"height", "wealth", "resource", "lake", "importance" };
		lang = selectSort(lang);

		for (String l : lang) {
			System.out.print(l+ " " );
		}

	}

}
