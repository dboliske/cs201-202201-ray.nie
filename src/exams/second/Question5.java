
//name: RUICHAO NIE  ;	course: CS201;	sec: 03
//date: 04/30/2022; 	name of project: exams.second

package exams.second;

import java.util.Scanner;

public class Question5 {

//	jump search use recursion
	public static int jumpsearch(double[] array, double value, int step, int end) {
		if (end < array.length && value > array[end]) {
			end += step;
			return jumpsearch(array, value, step, end);
		} else {
			for (int i = end - step + 1; i <= end && i < array.length; i++) {
				if (value == array[i]) {
					return i;
				}
			}

		}

		return -1;

	}

	public static void main(String[] args) {
		double[] numbers = { 0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142 };
		int step = (int) Math.sqrt(numbers.length);

		Scanner input = new Scanner(System.in);
		System.out.print("Please enter the number you wanna search : ");
		double value = Double.parseDouble(input.nextLine());
		int index = jumpsearch(numbers, value, step, 0);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.println("The value you search is " + value + " . Index is at " + index + ".");
		}

		input.close();
	}

}
