//name: RUICHAO NIE  ;	course: CS201;	sec: 03
//date: 04/30/2022; 	name of project: exams.second

package exams.second;

import java.awt.Polygon;

public class Rectangle extends Polygon {

	private double width;
	private double height;

	public Rectangle() {
		super();
		width = 1;
		height = 1;

	}

	/**
	 * @return the width
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(double width) {
		if (width > 0) {
			this.width = width;
		}
	}

	/**
	 * @return the height
	 */
	public double getHeight() {

		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(double height) {
		if (height > 0) {
			this.height = height;
		}

	}

	@Override
	public String toString() {
		return super.toString() + "Rectangle [width=" + width + ", height=" + height + "]";
	}

	public double area() {
		return (this.height * this.width);

	}

	public double perimeter() {
		return (2.0 * (this.height + this.width));

	}
	

}
