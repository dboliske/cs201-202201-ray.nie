// Ray Nie,CS201,Lab1,2022/01/29,exercise1

package labs.lab1;

import java.util.Scanner;

public class exercise1 {

	public static void main(String[] args) {
		// create a scanner 
		
		Scanner input = new Scanner(System.in);
		
		// prompt the user input 
		System.out.print("Enter your name: ");
		
		// read the user input 
		String str1=input.nextLine();
		
		// close the scanner 
		input.close();
		
		// output fun echo
		System.out.println("Echo:" + str1);
		

	}

}
