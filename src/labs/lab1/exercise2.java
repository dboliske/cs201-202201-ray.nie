// Ray Nie,CS201,Lab1,2022/01/29,exercise2

package labs.lab1;

public class exercise2 {

	public static void main(String[] args) {
		
		// define the basic data
		int rayage = 29;
		int papaage =59;
		
		double birthyear = 1992;
		int rayhight = 68;
		
		// My age subtracted from father age
		System.out.println("My age subtracted from father age is : " + (rayage-papaage));
		
		// My birthyear multiplied by 2
		System.out.println("My birthyear multiplied by 2 is :" + (birthyear/2));
		
		// height in inches to cms
		System.out.println("My hight is :" + (rayhight*2.54) + "cm" );
		
		// Convert height in inches to feet and inches where inches is an integer (mode operator)
		System.out.println("My hight is :" + (rayhight/12) + "feet " + (rayhight%12) + "inches");
		
	}

}
