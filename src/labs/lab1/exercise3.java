// Ray Nie,CS201,Lab1,2022/01/29,exercise3

package labs.lab1;

import java.util.Scanner;

public class exercise3 {

	public static void main(String[] args) {
		// create the scanner 
		Scanner input = new Scanner(System.in);
		
		// prompt the user input
		System.out.print("Enter Your First Name : ");
		char c = input.nextLine().charAt(0);
		
		// close scanner
		input.close();
		
		// output the first initial 
		System.out.print(c);

	}

}
