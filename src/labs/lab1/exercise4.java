// Ray Nie,CS201,Lab1,2022/01/29,exercise4
// test case 
// Input a temperature in Fahrenheit ：11/-10/10000000
// Temperature in Celsius is ：-11.666666666666666/-23.333333333333332/5555537.777777778
// Input a temperature in Celsius ：11/-30/-10000000000
// Temperature in Fahrenheit is ：51.8/-22.0/-1.7999999968E10

package labs.lab1;

import java.util.Scanner;

public class exercise4 {

	public static void main(String[] args) {
		// create a scanner for user input
		Scanner input = new Scanner(System.in);
		
		// prompt the user input
		System.out.print("Input a temperature in Fahrenheit ：");
		
		// read  and convert to double 
		double a = Double.parseDouble(input.nextLine()); 
		double b = (a-32)*5/9;
		System.out.println("Temperature in Celsius is ：" + b);
		
		// prompt the user input
		System.out.print("Input a temperature in Celsius ：");
		// read and convert to double 
		double c = Double.parseDouble(input.nextLine()); 
		double d = 9*c/5+32;
		System.out.print("Temperature in Fahrenheit is ：" + d);	
		
		input.close();
		
	}

}
