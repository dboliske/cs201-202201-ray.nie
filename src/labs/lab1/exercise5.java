// Ray Nie,CS201,Lab1,2022/01/29,exercise5
// test cases as followed
// Enter length in inches of a box: 1/ 2/ 123.2/ -1
// Enter length in inches of a box: 1/ 3/ 22.44/ -1
// Enter length in inches of a box: 1/ 6/ 12.22/ -1
// Amount of wood (square feet) needed to make the box is:0.5/ 6.0/ 757.3881333333334/ 0.5

package labs.lab1;

import java.util.Scanner;

public class exercise5 {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt the user input
		System.out.print("Enter length in inches of a box: ");
		double l = Double.parseDouble(input.nextLine());
		
		System.out.print("Enter width in inches of a box: ");
		double w = Double.parseDouble(input.nextLine());
		
		System.out.print("Enter depth in inches of a box: ");
		double h = Double.parseDouble(input.nextLine());
		
		// get the square
		double s = (2*l*w + 2*w*h + 2*l*h)/12;
		System.out.print("Amount of wood (square feet) needed to make the box is " + s + "feet");
		
		input.close();
		

	}

}
