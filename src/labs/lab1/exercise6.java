// Ray Nie,CS201,Lab1,2022/01/29,exercise6
// test cases as followed
// Enter a inches value : 11/22222/1
// The centimeters is : 27.94/56443.88/2.54

package labs.lab1;

import java.util.Scanner;

public class exercise6 {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt user to input 
		System.out.print("Enter a inches value : ");
		
		double i =Double.parseDouble(input.nextLine());
		double c = i*2.54;
		
		// Convert inches to centimeters and out put the result
		System.out.print("The centimeters is : " + c);
		
		input.close();
		
		

	}

}
