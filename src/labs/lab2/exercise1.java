// Ray Nie,CS201,Lab2,2022/02/03,exercise1

package labs.lab2;

import java.util.Scanner;

public class exercise1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a scanner 
		System.out.print("Enter the dim of the square : "); // prompt the user input the initial dim
		
		int size =Integer.parseInt(input.nextLine());// read the user input and transform 
		
		for(int row=0; row<size; row++) { //use for loop 
			for (int col=0; col<size; col++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		input.close();
	}

}
