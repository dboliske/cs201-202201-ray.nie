// Ray Nie,CS201,Lab2,2022/02/03,exercise2


package labs.lab2;

import java.util.Scanner;

public class exercise2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);// create a scanner
	
		double grade = 0;  // define the initial values
		double sum = 0; // define the initial values
		int i = 0; // define the initial values
		
		while(grade >=0 | grade == -1) {
			System.out.print("Input your grades and exit by inputting '-1': "); // prompt the user input
			grade = Double.parseDouble(input.nextLine()); // convert the value to double
			if (grade == -1) {    // if input -1 stop the loop
				break;
			}
			sum= sum + grade; 
			i++;
		}
		System.out.println("average is " + sum/i); // output the average
		input.close();

	}

}

