// Ray Nie,CS201,Lab2,2022/02/03,exercise3

package labs.lab2;

import java.util.Scanner;

public class exercise3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		boolean done = false; // define the boolean status
		
		do {
			System.out.println("Option 1: Say Hello");
			System.out.println("Option 2: Addition of 2 nums");
			System.out.println("Option 3: Multiplication of 2 nums");
			System.out.println("Option 4: Exit");	
			System.out.println("Input Your Choice: ");	
			char n =input.nextLine().charAt(0);	 // get what user input.
					
			switch(n) { // 4 options menu
			case '1':
				System.out.println("Say Hello");
				break;
				
			case '2':
					System.out.print("Please Enter 2 numbers one by one : ");
					Double i = Double.parseDouble(input.nextLine());
					
					System.out.print("Please Enter 2 numbers one by one : ");
					Double j = Double.parseDouble(input.nextLine());
					
					double s =i + j;
				    System.out.println("Addition of 2 nums is " + s);
				break;
				
			case '3':
				System.out.print("Please Enter 2 numbers one by one : ");
				Double x = Double.parseDouble(input.nextLine());
				
				System.out.print("Please Enter 2 numbers one by one : ");
				Double y = Double.parseDouble(input.nextLine());
				
				double m =x*y ;
			    System.out.println("Multiplication of 2 nums is " + m);
			break;
			
			case '4':
				 done = true;
				 break;
			default:
				 System.out.println("What?" );
			}
		}while(!done);
		
		input.close();
		
		System.out.println("Goodbye!");
	
	}
}

