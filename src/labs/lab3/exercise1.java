
// Ray Nie,CS201,Lab3 
// 2022/02/12,exercise1

package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class exercise1 {

	
	// You have been given a file called "src/labs/lab3/grades.csv".
	//  It contains a list of students and their exam grades. 
	// Write a program that reads in the file and computes the average grade for the class and then prints it to the console.
	
	public static void main(String[] args) throws IOException {
	
		
		// read the file 
		File f = new File("src/labs/lab3/grades.csv"); 
		
		Scanner input = new Scanner(f); 
		Double [] values = new Double[100];
		int count =0; 
		
		while(input.hasNextLine()) {  // judge the file row boolean status if yes then  go loop
			String line []=input.nextLine().split(","); // split the row and store in a array
			values[count]=Double.parseDouble(line[1]);  // only store the second value of the row 
			count++;	// count the actual array size
		}
		input.close();
		
		Double[] smaller = new Double [count];  // resize the array
		for (int i=0; i < count ; i++) {
			smaller[i]=values[i];
		}
		values = smaller ;
		smaller = null;
		
		// get the average
		Double sum = 0.0; 		
		for (int i =0; i < values.length;i++ ) {
			sum = sum + values[i];
		}
		System.out.println("The average is ： " + sum/values.length);
	}

}
