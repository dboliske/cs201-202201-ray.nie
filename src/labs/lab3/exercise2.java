
// Ray Nie,CS201,Lab3 
// 2022/02/12,exercise2

package labs.lab3;
//then prompts the user for a file name so that these values can be saved to that file. For example, 
// if the user enters "output.txt", then the program should write the numbers that have been read to "output.txt".


import java.io.FileWriter;
import java.util.Scanner;

public class exercise2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a scanner for input
		double [] values = new double[3]; // create a initial array
		boolean done = false; // create a flag for jump out the loop when do & while
		int count = 0;// create a initial counter for counting input times(array size)
		
		// classic user input and stop array coding below 
		do {  // use a do while loop for frequent prompt  user input
			try { // use try catch for better user experience for input
				System.out.print("Enter a number or Exit by input done :"); // prompt the user input 
				String line = input.nextLine(); // convert the input to string 
				
				if (line.equalsIgnoreCase("done")) {  // use equal to judge if the user input done 
					done = true; // if yes ,the done = true. break and jump to line 39;
					break;
				}
				
				if(count == values.length) {  // dynamic resize the array length , since it is hardly for ==. 
					double [] newvalues = new double[2*values.length]; //  use a bigger one to store the old one
					for (int i =0; i < values.length;i++) {
						newvalues[i] = values [i];	
					}
					values= newvalues; // let the old one array point to the new array memory
					newvalues = null;// recycle bigger one array
				}
				values[count] = Double.parseDouble(line); // whenever input one line , the store into the array
				count++;	// keep the counter recording the loop times
				
			}catch(Exception e) {
				System.out.print("The input is not a number or done word"); // catch the exception and prompt the user
			}	
			
		}while(!done); // boolean flag 
		
		
		// resize the array
		double [] smaller = new double[count]; 
		for (int i =0 ;i < count ; i++) {
			smaller	[i]	= values[i];
		}
		values = smaller;
		smaller = null;
		
		// write a file 
		System.out.println("Enter your file name :");
		String filename = input.nextLine();	
		try {
			FileWriter f = new FileWriter ("src/labs/lab3/"+ filename +".txt");
			
			for (int i=0; i<values.length; i++) {
				f.write(values[i] + "\n");
			}
			f.flush();
			f.close();	
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		input.close();
		System.out.println("Finished Writing,Refresh Your File");
	}

}
