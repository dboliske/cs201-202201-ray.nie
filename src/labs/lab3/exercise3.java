package labs.lab3;

//Ray Nie,CS201,Lab3 
//2022/02/12,exercise2


// Write a program that will find the minimum value and print it to the console for the given array:
public class exercise3 {

	public static void main(String[] args) {
	
		int [] a = new int [] {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};

		int min = a[0]; // define the first value is min 
		for (int i=0; i<a.length;i++) { // use loop to compare with every value 
			if (a[i] < min) {
				min = a[i];
			}
		}
		System.out.println(min);
	}

}
