
// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/20 ,Program Name: GeoLocation


package labs.lab4;


public class GeoLocation {
	
	// Create two instance variables, lat and lng, both of which should be doubles.
	private double lat;
	private double lng;

	
	// Write the default constructor.
	public GeoLocation(){
		lat = 0;
		lng = 0;
	}
	
	// Write the non-default constructor.
	public GeoLocation(double lat, double lng){
		this.lat = lat;
		this.lng = lng;
	}

	// Write 2 accessor methods, one for each instance variable.
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	// Write 2 mutator methods, one for each instance variable.
	public void setLat(double lat) {
		this.lat = lat;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}

	// Write a method that will return the location in the format "(lat, lng)" (the toString method).
	public String toString() {
		return "(" + lat + "," + lng + ")";
	}
	
	// Write a method that will return true if the latitude is between -90 and +90.
	public boolean compareLat() {
		return this.getLat() > -90 && this.getLat() < 90 ;
	}
	
	// Write a method that will return true if the longitude is between -180 and +180.
	public boolean compareLng() {
		return this.getLng() > -180 && this.getLng() < 180 ;
	}
	
	// Write a method that will compare this instance to another GeoLocation (the equals method).
	public boolean equals(GeoLocation wr) {
		return this.lat == wr.getLat() && this.lng == wr.getLat();
		
	}	
	
}
