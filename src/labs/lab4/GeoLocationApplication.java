

// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/20 ,Program Name: GeoLocationApplication




package labs.lab4;

public class GeoLocationApplication {

	public static void main(String[] args) {
		//Now write an application class that instantiates two instances of GeoLocation. 
		//One instance should use the default constructor and the other should use the non-default constructor.
		//Display the values of the instance variables by calling the accessor methods.
		
		// use a default constructor
		GeoLocation wr = new GeoLocation();
		System.out.println(wr);
		
		//  use non- default constructor
		GeoLocation wrr = new GeoLocation(-80,150);
		System.out.println(wrr.compareLat());
		System.out.println(wrr.compareLng());
		System.out.println(wrr);
		
		// compare two instance
		System.out.println(wr.equals(wrr));

	}

}
