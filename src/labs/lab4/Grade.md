# Lab 4

## Total

21/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        5/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        5/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        5/8
  * Application Class   1/1
* Documentation         3/3

## Comments

1. Your non-default constructors do not validate the parameters prior to assigning them to the instance variables
2. Your mutator methods do not validate the parameters prior to assigning them to the instance variables
3. Your validation methods do not follow the UML diagrams