

// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/20 ,Program Name: PhoneNumber



package labs.lab4;


// Create three instance variables, countryCode, areaCode and number, all of which should be Strings.
public class PhoneNumber {
	
	private String countryCode;
	private String areaCode;
	private String number;
	
	
	// Write the default constructor.
	public PhoneNumber(){
		countryCode = "0000";
		areaCode = "000";
		number ="0000000";
	}
	
	// Write the non-default constructor.
	public PhoneNumber(String countryCode,String areaCode,String number ){
		this.countryCode = countryCode ;
		this.areaCode = areaCode ;
		this.number = number ;
	}

	// Write 3 mutator methods, one for each instance variable.
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	// Write 3 accessor methods, one for each instance variable.
	public String getCountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	// Write a method that will return the entire phone number as a single string (the toString method).
	public String toString() {
		return countryCode + "-" + areaCode + "-" + number;	
	}
	
	// Write a method that will return true if the areaCode is 3 characters long.
	public boolean areaCodeLength() {
		return areaCode.length() == 3;
	}
	
	// Write a method that will return true if the number is 7 characters long.
	public boolean numberLength() {
		return number.length() == 7;
	}

	// Write a method that will compare this instance to another PhoneNumber (the equals method).
	public boolean equals(PhoneNumber wr) {
		return areaCodeLength() && numberLength() && this.countryCode.equals(wr.getCountryCode()) && this.areaCode.equals(wr.getAreaCode())&& this.number.equals(wr.getNumber());
	}
	
	
}
