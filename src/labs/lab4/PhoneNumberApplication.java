

// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/20 ,Program Name: PhoneNumberApplication


package labs.lab4;

public class PhoneNumberApplication {

	public static void main(String[] args) {
		// default constuctor
		PhoneNumber wr = new PhoneNumber();
		System.out.println(wr);
		
		// default non-constuctor	
		PhoneNumber wrr = new PhoneNumber("0086","185","6580537");
		System.out.println(wr.equals(wrr));
		
		// tostring method
		System.out.println(wrr.toString());
	}

}
