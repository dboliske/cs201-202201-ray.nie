
// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/20 ,Program Name: Potion

package labs.lab4;

public class Potion {

	private String name;
	private double strength;
	
	public Potion() {
		name = "null";
		strength = 0.1;
	}
	
	public Potion(String name, double strength) {
		this.name = name;
		this.strength = strength;
	}
	
	
	
	public String getName() {
		return name;
	}
	
	
	public double getStrength() {
		return strength;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setStrength(double strength) {
		this.strength = strength;
	}
	
	
	public String toString() {
		return "The Potion' name is " + name + ", the strength is " + strength;
	}

	
	public boolean compareStrength() {
		return this.strength > 0 && this.strength < 10;
	}
	
	public boolean equals(Potion wr) {
		return this.name.equals(wr.getName()) && this.strength == wr.getStrength();
	}
		
}
