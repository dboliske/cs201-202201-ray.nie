
// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/20 ,Program Name: PotionApplication



package labs.lab4;

public class PotionApplication {

	public static void main(String[] args) {
		
		// default constructor
		Potion wr = new Potion() ;
		System.out.println(wr.toString());
		
		// non-default constructor and toString method
		Potion wrr = new Potion("Fever",8);
		System.out.println(wr.equals(wrr));
		System.out.println(wrr.toString());

	}

}
