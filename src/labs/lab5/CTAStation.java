package labs.lab5;

public class CTAStation  extends GeoLocation{
	
	
	// Create a class that will implement a CTAStation, which should inherit from GeoLocation, 
	// with the UML diagram below.
	
	
	// subclass variable 
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	// default constructor 
	
	public CTAStation() {
		super();
		this.name = "null";
		this.location = "null";
		this.wheelchair = false;
		this.open = false;
		
	}
	
	// non-default constructor 
	public CTAStation(String name, double lat,double lng, String location, boolean wheelchair , boolean open ) {
		super(lat,lng);
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}
	
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	@Override
	public String toString() {
		return "This " + super.toString() + " nearest with " + name + " . " +"The location is " + location + " . " + " The wheelchair is " +  wheelchair + " . " + "The open is " + open ; 				
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(super.equals(obj))) {
			return false;
		}else if (!(obj instanceof CTAStation)) {
			return false;
		}
		
		CTAStation c = (CTAStation)obj;
		
		return this.name.equals(c.getName()) && this.location.equals(c.getLocation()) && this.wheelchair == c.hasWheelchair() && 
				this.isOpen()==c.isOpen() && this.getLat() == c.getLat() && this.getLng()== c.getLng();
		
	}
	
	
	
}
