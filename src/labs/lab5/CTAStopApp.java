package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {
	
	
	// read the file 
	
	public static CTAStation[] readFile(String filename) {
		CTAStation[] station = new CTAStation[10];
		int count = 0;
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			input.nextLine();
			// end of file loop 
			while(input.hasNextLine()) {
				
					String line = input.nextLine();
					String[] values =line.split(",");
					String name = values[0];
					double lat = Double.parseDouble(values[1]);
					double lng = Double.parseDouble(values[2]);
					String location = values[3];
					boolean wheelchair  = Boolean.parseBoolean(values[4]);
					boolean open  = Boolean.parseBoolean(values[5]);
					if(station.length == count) {
						station = resize (station,station.length*2 );
					}
					station[count] = new CTAStation(name,lat,lng,location,wheelchair,open);
					count++;

			}
		input.close();
		}catch (FileNotFoundException fnf) {
			System.out.println("No file found.");
		}catch (Exception e) {
			System.out.println("Error in reading the file ");
		}
		
		station = resize(station, count);
        return station;
	}
	
	
	
	//resize the array
	 public static CTAStation[] resize(CTAStation[] data, int size) {
	        CTAStation[] temp = new CTAStation[size];
	        int limit = data.length > size ? size : data.length;
	        for (int i = 0; i < limit; i++) {
	            temp[i] = data[i];
	        }
	        return temp;
	    }
	 
	 
	 
	 // menu
	
	 public static void menu(CTAStation[] station) {
		 
		 boolean done = false;
		 
		 Scanner input = new Scanner(System.in);
		 do {
			    System.out.println("1. Display Station Names");
	            System.out.println("2. Display Stations with/without Wheelchair access");
	            System.out.println("3. Display Nearest Station");
	            System.out.println("4. Exit");
	            System.out.print("Choice: ");

	            String choice = input.nextLine();
	            switch (choice) {
	                case "1":
	                    displayStationNames(station);
	                    break;
	                case "2":
	                    displayByWheelChair(input, station);
	                    break;
	                case "3":
	                    displayNearest(input, station);
	                    break;
	                case "4":
	                    done = true;
	                    break;
	                default:
	                    System.out.println("I dont know what is your choice");
	                }			 
		 }while(!done);
		 
		 System.out.println("Goodbye!");
		
	 }

	 
	 
	 // displayStationNames:
	 
	 public static void displayStationNames(CTAStation[] data) {
		 for (int i = 0; i< data.length; i++) {
			 System.out.println("The name is " + data[i].getName());
		 }	 
	 }
	 
	 //  displayByWheelChair
	 
	 public static void  displayByWheelChair(Scanner input, CTAStation[] data) {
		 
		 boolean done = false;
		 boolean hasfound = false;
		 
		do {
			System.out.println("input 'y' or 'n' for wheelchair choice : )");
			 String line = input.nextLine()	;
		
			 
			 
			switch(line.toLowerCase()) {
			 case"y":
				
				 for (int i=0; i<data.length; i++) {
                     if (data[i].hasWheelchair()) {
                         System.out.println(data[i]);
                         hasfound = true;
                     }
                 }
				 
                 done = true;
                 break;
				 
			 case"n":
		
				 for (int i=0; i<data.length; i++) {
                     if (!data[i].hasWheelchair()) {
                         System.out.println(data[i]);
                         hasfound = true;
                     }
                 }
                 done = true;
				 break;
				 
			default:
				 System.out.println("I dont know what you input .");
				 }	
			  
			 }while(!done);
		
		if(!hasfound) {
			 System.out.println("No Stations are found");
		 }
			 
		 
	 }
	 
	 
	 // displayNearest:
	 
	 public static void displayNearest(Scanner input, CTAStation[] data) {
		 
		 boolean done = false;

		double lat = 0;
		double lng = 0;
		
			do {
				try {
					System.out.println("Please input latitude: ");
					 lat = Double.parseDouble(input.nextLine()); 
					
					System.out.println("Please input longitude: ");
					lng = Double.parseDouble(input.nextLine()); 
					
					// 跳过183；					
					done = true;
					
				}catch(Exception e) {
					System.out.println("I dont know what you input .");
		
				}		
			}while(!done);
			
			//  有问题
			
			int count = 0;
			for(int i=0; i<data.length; i++) {
				if (data[i].calcDistance(lat,lng)<data[count].calcDistance(lat,lng)) {
					count=i;
				}
//				System.out.println(data[i].calcDistance(lat,lng));
//				System.out.println(data[count].calcDistance(lat,lng));
			}
//			System.out.println(count);
			
			System.out.println("The nearest station is "+ data[count].getName());
 
	 }
	
	 
	 
	public static void main(String[] args) {
		// read file 
		
		CTAStation[]  data = readFile("src/labs/lab5/CTAStops.csv");
		menu (data);
		
		// menu
		
		

	}

}
