
// Name: Ray Nie, Course:CS201 , Sec#03 
// Date: 2022/02/20 ,Program Name: GeoLocation


package labs.lab5;


public class GeoLocation {
	
	// Create two instance variables, lat and lng, both of which should be doubles.
	private double lat;
	private double lng;

	
	// Write the default constructor.
	public GeoLocation(){
	
	}
	
	// Write the non-default constructor.
	public GeoLocation(double lat, double lng){
//		lat = 0; 
		// ****** non-default constructors do not validate the parameters prior to assigning them to the instance variables
		setLat(lat);
//		lng = 0;
		// ****** non-default constructors do not validate the parameters prior to assigning them to the instance variables
		setLng(lng);
	}
	

	// Write 2 accessor methods, one for each instance variable.
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	// Write 2 mutator methods, one for each instance variable.
	public void setLat(double lat) {
	// ***** mutator methods do not validate the parameters prior to assigning them to the instance variables
		if (this.validLat(lat)) {
			this.lat = lat;
		}
		
	}
	public void setLng(double lng) {
	// ***** mutator methods do not validate the parameters prior to assigning them to the instance variables
		if (this.validLng(lng)) {
			this.lng = lng;
		}
	}
	

	public double calcDistance(double lat , double lng) {
		return  Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
	}
	
	public double calcDistance(GeoLocation g) {
		return this.calcDistance(g.getLat(), g.getLng());
//	 return  Math.sqrt(Math.pow(this.lat - g.getLat(), 2) + Math.pow(this.lng - g.getLng(), 2));
	}
	
	
	
	// Write a method that will return the location in the format "(lat, lng)" (the toString method).
	public String toString() {
		return "(" + lat + "," + lng + ")";
	}
	
	// Write a method that will return true if the latitude is between -90 and +90.
	public boolean validLat(double lat) { // need parameters in （）；
		return lat > -90 && lat < 90 ;
	}
	
	// Write a method that will return true if the longitude is between -180 and +180.
	public boolean validLng(double lng) { // need parameters in （）；
		return lng > -180 && lng < 180 ;
	}
	
	// Write a method that will compare this instance to another GeoLocation (the equals method).
	public boolean equals(GeoLocation wr) {
		return this.lat == wr.getLat() && this.lng == wr.getLat();
		
	}	
	
}
