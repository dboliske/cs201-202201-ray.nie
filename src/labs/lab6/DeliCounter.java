package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliCounter {

	public static void main(String[] args) {

		// Create a ArrayList and Scanner
		Scanner input = new Scanner(System.in);
		ArrayList<String> deli = new ArrayList<>();
		menu(input, deli);

	}

	// Create a menu
	public static void menu(Scanner input, ArrayList<String> deli) {

		String[] choice = { "Add Customer to Queue", "Help customer", "Exit" };
		boolean done = false;
		do {
			for (int i = 0; i < choice.length; i++) {
				System.out.println((i + 1) + "." + choice[i]);
			}
			System.out.println("What is your Choice: ");
			String option = input.nextLine();
			switch (option) {
			case "1":
				addQueue(input, deli);
				break;
			case "2":
				helpeUser(deli);
				break;
			case "3":
				done = true;
				System.out.println("Good Bye!!!");
				break;
			default:
				System.out.println("i dont know what you mean!!!");
			}
		} while (!done);
	}

// Add Customer to Queue method

	public static void addQueue(Scanner input, ArrayList<String> deli) {
		System.out.println("Input your name : ");
		String name = input.nextLine();
		deli.add(name);
		System.out.println("Your position is : " + (deli.indexOf(name)+1));

	}

	// Help customer method

	public static void helpeUser(ArrayList<String> deli) {
		if (deli.size() == 0) {
			System.out.println("No front customer !!!");
		} else {
			String frontUser = deli.get(0);
			System.out.println("The front user is : " + frontUser);
			deli.remove(0);
			
		}

	}

}
