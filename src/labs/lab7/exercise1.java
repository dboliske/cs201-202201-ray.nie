package labs.lab7;

public class exercise1 {

	// bubble sort method

	public static int[] bubbleSort(int[] a) {
		boolean done = false;
		do {
			done = true;
			for (int i = 0; i < a.length - 1; i++) {
				if (a[i + 1] < a[i]) {
					int temp = a[i + 1];
					a[i + 1] = a[i];
					a[i] = temp;
					done = false;
				}

			}

		} while (!done);

		return a;

	}

	// main method

	public static void main(String[] args) {
		int[] array = { 10, 4, 7, 3, 8, 6, 1, 2, 5, 9 };

		bubbleSort(array);

		for (int z : array) {
			System.out.print(z + ",");
		}

	}
}
