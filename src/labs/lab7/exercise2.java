package labs.lab7;

public class exercise2 {

	// insert sort method

	public static String[] insertSort(String[] array) {

		for (int j = 1; j < array.length; j++) {
			int i = j;
			while (i > 0 && array[i].compareTo(array[i - 1]) < 0) {
				String temp = array[i];
				array[i] = array[i - 1];
				array[i - 1] = temp;
				i--;
			}

		}

		return array;

	}

	// main method

	public static void main(String[] args) {
		String[] lang = { "cat", "fat", "dog", "apple", "bat", "egg" };

		insertSort(lang);

		for (String l : lang) {
			System.out.print(l + " ");
		}

	}

}
