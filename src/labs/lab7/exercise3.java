package labs.lab7;

public class exercise3 {

//	selectSort method
	public static double[] selectSort(double[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			int min = i;
			for (int j = i + 1; j < array.length; j++) {
				if (array[j] < array[min]) {
					min = j;
				}
			}

			if (min != i) {
				Double temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}

		return array;
	}

//	main

	public static void main(String[] args) {
		double[] a = { 3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282 };

		selectSort(a);

		for (double z : a) {
			System.out.print(z + ", ");
		}
	}

}
