package labs.lab7;

import java.util.Arrays;
import java.util.Scanner;

public class exercise4 {
//Binary Search algorithm method

	public static int binarySearch(String[] l, int start, int last, String lang) {

		int mid = (start + last) / 2;

		if (lang.equalsIgnoreCase(l[mid])) {
			return mid;
		} else if (lang.compareToIgnoreCase(l[mid]) > 0) {
			start = start + 1;

		} else {
			last = last - 1;

		}
		
		if (start <= last && lang != l[mid]) {
			mid = binarySearch(l, start, last, lang);
		} else {
			mid = -1;
		}

		return mid;
	}

//	main
	public static void main(String[] args) {

		String[] l = { "c", "html", "java", "python", "ruby", "scala" };
		
		Arrays.sort(l);

		System.out.println("Enter a word : ");

		Scanner input = new Scanner(System.in);
		String language = input.next();

		int mid = binarySearch(l, 0, l.length - 1, language);

		if (mid == -1) {
			System.out.println(language + " not found.");
		} else {
			System.out.println(language + " found at index " + mid + ".");
		}

	}

}
