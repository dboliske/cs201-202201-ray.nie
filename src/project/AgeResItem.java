      
package project;
public class AgeResItem extends Item {

    private int age;

    public AgeResItem() {
        super();
    }

    public AgeResItem(String name, double price, int age) {
        super(name, price);
        setAgeRes(age);
    }

    public double getAgeRes() {
        return age;
    }

    public void setAgeRes(int age) {
        if (age >= 1) {
            this.age = age;
        }
    }

    public boolean ageRes(int data) {

        return data > age;

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AgeResItem other = (AgeResItem) obj;
        return Double.doubleToLongBits(age) == Double.doubleToLongBits(other.age);
    }

    @Override
    public String toString() {
        return "AgeResItem [AgeRes=" + age + "]";
    }

    public String toCSV() {
        return "AgeResItem," + super.csvData() + "," + age;
    }

}

    