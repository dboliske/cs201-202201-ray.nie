      
package project;
import java.util.Objects;

public class Item {
	private String name;
	private double price;
	
	public Item() {
	}
	
	public Item(String name, double price) {
		this.name = name;
		setPrice(price);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if(price>=0) {
			this.price = price;
		}
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		return Objects.equals(name, other.name)
				&& Double.doubleToLongBits(price) == Double.doubleToLongBits(other.price);
	}
	
	

	@Override
	public String toString() {
		return "Item [name=" + name + ", price=" + price + "]";
	}

	protected String csvData() {
		return name + "," + price ;
	}
	
	public String toCSV() {
		return "Item," + csvData();
	}

}

    