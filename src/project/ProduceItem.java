package project;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class ProduceItem extends Item {

    private Date expdate;

    public ProduceItem() {
        super();
    }

    public ProduceItem(String name, double price, Date date) {
        super(name, price);
        this.expdate = date;
    }

    public Date getDate() {
        return expdate;
    }

    public void setDate(Date date) {
        this.expdate = date;
    }

    public boolean isExp() {
        return expdate.compareTo(new Date()) < 0; // 前面的时间和后面的时间比较，前面比后面小为-1；
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProduceItem other = (ProduceItem) obj;
        return Objects.equals(expdate, other.expdate);
    }

    @Override
    public String toString() {
        return super.toString() + "ProduceItem [date=" + expdate + "]";
    }

    public String dateToStr(Date date) {

        SimpleDateFormat FORMAT = new SimpleDateFormat("MM/dd/yyy");

        String str = FORMAT.format(expdate);

        return str;
    }

    public String toCSV() {

        return "ProduceItem," + super.csvData() + "," + dateToStr(expdate);

    }
}

    