package project;
public class ShevledItem extends Item {

	public ShevledItem() {
		super();
	}

	public ShevledItem(String name, double price) {
		super(name, price);

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + "ShevledItem []";
	}

	public String toCSV() {

		return "ShevledItem," + super.csvData();

	}

}
