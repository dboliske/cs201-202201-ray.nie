package project;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class StoreAPP {

    //	menu
    static ArrayList<Item> cart = new ArrayList<>();
    static final String filename = "src/project/stock-new.csv";

    public static ArrayList<Item> menu(Scanner input, ArrayList<Item> data) throws IOException {
        boolean done = false;
        System.out.println("Hello! Welcome to Self-Helped Store!!! ");
        System.out.println("------------*******------------- ");

        String[] options = {"Create the item for store", "Remove the items from store",
            "Search and buy the items from store", "Modify the items from store", "Show the cart", "Exit"};

        do {
            for (int i = 0; i < options.length; i++) {
                System.out.println((i + 1) + ". " + options[i]);
            }
            System.out.println("------------*******------------- ");
            System.out.println("Pleas enter the option number :");

            String choice = input.nextLine();

            switch (choice) {
                case "1": // "Create the item for store"
                    createItem(data, input);
                    break;
                case "2":// "Remove the items from store"
                    System.out.println("please input item name:");
                    String deleteItemName = input.nextLine();
                    Item item = removeItem(data, deleteItemName);
                    if (item != null) {
                        System.out.println("successfully deleted item:" + deleteItemName);
                    } else {
                        System.out.println(deleteItemName + " not found");
                    }
                    break;
                case "3":// "Search and buy the items from store"
                    searchItem(data, input);
                    break;
                case "4":// "Modify the items from store"
                    modifyItem(data, input);
                    break;
                case "5":
                    if (cart.size() == 0) {
                        System.out.println("the cart is empty");
                    }
                    for (Item a : cart) {
                        System.out.println(a.toCSV());
                    }
                    break;
                case "6"://"EXIT"
                    done = true;
                    break;
                default:
                    System.out.println("I am sorry , i didn't understand that.");

            }

        } while (!done);

        return data;
    }

    //	read file
    public static ArrayList<Item> readFile(String filename) {
        ArrayList<Item> items = new ArrayList<>();

        try {

            File f = new File(filename);
            Scanner input = new Scanner(f);

            // end - of - file loop

            while (input.hasNextLine()) {
                try {
                    String line = input.nextLine();
                    String[] values = line.split(",");
                    Item a = null;

                    switch (values[0]) {
                        case "ProduceItem":
                            SimpleDateFormat simpledate = new SimpleDateFormat("MM/dd/yyyy");
                            simpledate.parse(values[3]);
                            a = new ProduceItem(values[1], Double.parseDouble(values[2]), simpledate.parse(values[3]));
                            break;

                        case "ShevledItem":
                            a = new ShevledItem(values[1], Double.parseDouble(values[2]));
                            break;

                        case "AgeResItem":
                            a = new AgeResItem(values[1], Double.parseDouble(values[2]), Integer.parseInt(values[3]));
                            break;
                    }

                    items.add(a);

                } catch (Exception e) {
                    System.out.println("error!!! in while loop ");
                }

            }
            input.close();

        } catch (FileNotFoundException fnf) {

        } catch (Exception e) {
            System.out.println("Error occurred reading in file.");
        }
        return items;
    }

    // save file

    public static void saveFile(String filename, ArrayList<Item> data) {
        try {
            FileWriter writer = new FileWriter(filename);

            for (Item a : data) {
                writer.write(a.toCSV() + "\n");
                writer.flush();
            }

            writer.close();
        } catch (Exception e) {
            System.out.println("unknown exception happened ");
        }
    }

    // creatItem

    public static ArrayList<Item> createItem(ArrayList<Item> data, Scanner input) throws IOException {
        Item a = null;
        double price = 0;
        // the user choice for creating new item
        String[] options = {"Produce Item", "Shevled Item", "Age Restricted Item"};

        for (int i = 0; i < options.length; i++) {
            System.out.println((i + 1) + ". " + options[i]);
        }

        System.out.println("------------*******------------- ");

        System.out.print("Choose item number that you wanna add into the stock : ");

        String choice = input.nextLine();

        switch (choice) {
            case "1": // Produce Item
                System.out.print("Produce Item Name: ");
                String name = input.nextLine();

                try {
                    System.out.print("Input Produce Item Price: ");
                    price = Double.parseDouble(input.nextLine());
                } catch (Exception e) {
                    System.out.println("That is not a valid price. Returning to Create Option .");
                    return data;
                }

                System.out.print("Input Produce Item expiration date (M/D/Y): ");
                String date = input.nextLine();
                SimpleDateFormat simpledate = new SimpleDateFormat("MM/dd/yyyy");

                try {
                    a = new ProduceItem(name, price, simpledate.parse(date));
                } catch (ParseException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                System.out.println("------------*******------------- ");
                System.out.println("Successfully Create a new produce item! ");
                System.out.println(" ");
                break;

            case "2":// ShevledItem
                System.out.print("Input ShevledItem Name: ");
                String name2 = input.nextLine();

                try {
                    System.out.print("Input ShevledItem Price: ");
                    price = Double.parseDouble(input.nextLine());
                } catch (Exception e) {
                    System.out.println("That is not a valid price. Returning to Create Option .");
                    return data;
                }

                a = new ShevledItem(name2, price);

                System.out.println("------------*******------------- ");
                System.out.println("Successfully Create a new Shevled item! ");
                System.out.println(" ");
                break;

            case "3":// AgeResItem
                System.out.print("Input Age Restricted Item Name: ");
                String name3 = input.nextLine();

                try {
                    System.out.print("Input Age Restricted Item Price: ");
                    price = Double.parseDouble(input.nextLine());
                } catch (Exception e) {
                    System.out.println("That is not a valid price. Returning to Create Option .");
                    return data;
                }

                int age = 0;
                try {
                    System.out.print("Input Age Restricted Item Required Age : ");
                    age = Integer.parseInt(input.nextLine());
                } catch (Exception e) {
                    System.out.println("That is not a valid age. Returning to Create Option .");
                    return data;
                }

                a = new AgeResItem(name3, price, age);

                System.out.println("------------*******------------- ");
                System.out.println("Successfully Create a new AgeRes item! ");
                System.out.println(" ");

                break;

            default:
                System.out.println("That is not a valid item type. Returning to menu.");
                return data;
        }
        data.add(a);
        saveFile(filename, data);
        return data;
    }

    // Remove the items from store
    public static Item removeItem(ArrayList<Item> data, String name) {
        Item b = null;
        Iterator<Item> itraIterator = data.iterator();

        boolean founded = false;
        while (itraIterator.hasNext()) {
            Item item = itraIterator.next();
            if (item.getName().equalsIgnoreCase(name)) {
                itraIterator.remove();
                b = item;
                founded = true;
            }
        }
        saveFile(filename, data);

        return b;
    }

    // search the item in the store

    public static String searchItem(ArrayList<Item> data, Scanner input) {
        System.out.println("Please input what you wanna search: ");
        String name = input.nextLine();
        String c = name + " is not in the store";
        boolean founded = false;
        for (int i = 0; i < data.size(); i++) {
            Item b = data.get(i);

            if (b.getName().equalsIgnoreCase(name)) {
                founded = true;
                c = name + " is in the Store";
                break;
            }
        }

        System.out.println(c);

        if (founded) {

            System.out.println("add to the cart? (Y/N)");
            String choice = input.nextLine();

            if (choice.equalsIgnoreCase("y")) {
                addCart(data, name);
            }


        }
        return c;
    }

    // "Modify the items from store"
    public static ArrayList<Item> modifyItem(ArrayList<Item> data, Scanner input) {
        System.out.println("Input the item name you wanna modify: ");
        String name = input.nextLine();
        Item a = null;

        for (Item item : data) {
            if (item.getName().equalsIgnoreCase(name)) {
                a = item;
            }
        }

        if (a == null) {
            System.out.println("No such item in the store , you dont have to modify");
            return data;
        }

        if (a instanceof ProduceItem) {
            System.out.println("Now you can input new name,price,expiration date,like: banana,7.0, 05/07/2022");
            String line1 = input.nextLine();
            String[] values1 = line1.split(",");
            for (Item item : data) {
                if (item.getName().equalsIgnoreCase(name)) {
                    item.setName(values1[0]);
                    item.setPrice(Double.parseDouble(values1[1]));
                    SimpleDateFormat date2 = new SimpleDateFormat("MM/dd/yyyy");
                    try {
                        ((ProduceItem) item).setDate(date2.parse(values1[3]));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (a instanceof AgeResItem) {
            System.out.println(name + " you wanna modify is a Age Restriction Item");
            System.out.println("Now you can input new name,price,age,like: wine,20.0,23 ");
            String line2 = input.nextLine();
            String[] values1 = line2.split(",");

            for (Item item : data) {
                if (item.getName().equalsIgnoreCase(name)) {
                    item.setName(values1[0]);
                    item.setPrice(Double.parseDouble(values1[1]));
                    ((AgeResItem) a).setAgeRes(Integer.parseInt(values1[2]));
                }
            }
        } else if (a instanceof ShevledItem) {
            System.out.println(name + " you wanna modify is a Shevled Item");
            System.out.println("Now you can input new name,price,like: pen,15.0");
            String line3 = input.nextLine();
            String[] values1 = line3.split(",");

            for (Item item : data) {
                if (item.getName().equalsIgnoreCase(name)) {
                    item.setName(values1[0]);
                    item.setPrice(Double.parseDouble(values1[1]));
                }
            }
        }

        System.out.println("successfully modify item:" + name);
        saveFile(filename, data);
        return data;
    }

    public static void addCart(ArrayList<Item> data, String name) {
        Item c = removeItem(data, name);

        if (c == null) {
            System.out.println("There is no such thing in the store .");
        }

        cart.add(c);
        System.out.println("Successfully add:" + c.getName() + " from your cart!");
    }

    // delete the cart

    public static Item delCart(ArrayList<Item> data, Scanner input) {
        System.out.println("Please input item name you wanna delete from cart: ");

        Item delItem = null;

        String delItemName = input.nextLine();

        Iterator<Item> itraIterator = data.iterator();

        while (itraIterator.hasNext()) {
            Item item = (Item) itraIterator.next();
            if (item.getName().equalsIgnoreCase(delItemName)) {
                itraIterator.remove();
                delItem = item;
            }
        }
        System.out.println("Successfully delete" + delItemName + " from your cart!");
        return delItem;

    }

    public static void main(String[] args) throws IOException {

        Scanner input = new Scanner(System.in);

        ArrayList<Item> data = readFile(filename);
        menu(input, data);
        input.close();

        System.out.println("GoodBye!");

    }

}

    